<?php

class Building{
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function getAddress(){
		return $this->address;
	}

	public function printName(){
		return "The name of the building is $this->name.";
	}

	public function printFloors(){
		return "The $this->name has $this->floors floors.";
	}

	public function printAddress(){
		return "The $this->name is located at $this->address.";
	}

	public function printRename(){
		return "The name of the building has been changed to $this->name.";
	}
}	

class Condominium extends Building{
	
};

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');
$condominium = new Building('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');



?>
