<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 4</title>
</head>

	<body>

		<h1>Building</h1>
			<p><?php echo $building->printName(); ?></p>
			<p><?php echo $building->printFloors(); ?></p>
			<p><?php echo $building->printAddress(); ?></p>

		
			<p><?php $building->setName('Caswynn Complex'); ?></p>
			<p><?php echo $building->printRename(); ?></p>

		<h1>Condominium</h1>
			<p><?php echo $condominium->printName(); ?></p>
			<p><?php echo $condominium->printFloors(); ?></p>
			<p><?php echo $condominium->printAddress(); ?></p>

		
			<p><?php $condominium->setName('Enzo Tower'); ?></p>
			<p><?php echo $condominium->printRename(); ?></p>

	</body>

</html>
